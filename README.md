A simple lightweight screen autorotation/brightness daemon for the desktops that lack the functionality. 

**Dependencies**

* iio-sensor-proxy
* glib-2.0 \*
* gio-2.0 \*
* meson \*
* ninja \*

\* Build-time dependencies

**Building**

```bash
$ meson build
$ ninja -C build
$ sudo ninja -C build install
```

Default installation path is `/usr/local/bin`, please make sure it's in your $PATH.

**Running**

* Make sure iio-sensor-proxy is running (most distros can launch it as a service).
* run `sensor-daemon`.

**Configuration**

The program looks for the config file in current directory, then user's $XDG_CONFIG_DIR/sensor-daemon (usually .config) and /etc/sensor-daemon. The file name is `sensor-daemon.conf`.

The format is `name=value`. `%s` and `%d` are accepted for inserting values.

The config options are as follows:

* *RotateScreenCmd*: the command for rotating the screen, depends on window manager/compositor. 

   Ex. for xRandR: `RotateScreenCmd=xrandr --output eDP-1 --rotate %s`

   Ex. for Sway: `RotateScreenCmd=swaymsg output eDP-1 transform %s`

* *RotateScreenOptions*: the semicolon-separated list of four options for the display's side. The order is always normal;left;right;inverted. Fill these options as required by  your window manager/compositor.

   Ex. for xRandR: `RotateScreenOptions=normal;left;right;inverted`
   Ex. for Sway: `RotateScreenOptions=0;90;180;270`

   You can also swap the options as necessary in case the accelerometer's directions don't correlate to the screen's. E.g. "180;270;90;0".

* *SetBrightnessCmd*: command for setting the screen brightness. Depends on your video driver/ utilities for that.

   Ex. for intelbacklight: `SetBrightnessCmd=intelbacklight -set %d`

* *MinBrightness*, *MaxBrightness*: minimum and maximum values for your monitor's brightness. Depend on your video driver/monitor settings. Please refer to your brightness tool to get them.

* *AutoRotation*, *AutoBrightness*: Enables/disables auto rotation at the start of the program. Set to `true` to enable.

**Toggling the options**

Auto rotation/auto brightness toggles can be controlled via DBus. 

* The device name is `net.test.SensorDaemon`.
* The object name is `/net/test/SensorDaemonObject`
* The function names are `net.test.SensorDaemonControlInterface.ToggleAutoRotation` and `net.test.SensorDaemonControlInterface.ToggleAutoBrightness`

Example for gdbus:

```bash
#!/bin/bash
gdbus call -e -d net.test.SensorDaemon -o /net/test/SensorDaemonObject -m net.test.SensorDaemonControlInterface.ToggleAutoRotation
```

