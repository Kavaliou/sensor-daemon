#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <glib.h>
#include <gio/gio.h>
#include <wordexp.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

/** DBUS introspection data */
static const gchar *introspection_xml =
  "<node>"
  "   <interface name='net.test.SensorDaemonControlInterface'>"
  "       <annotation name='org.gtk.GDBus.Annotation' value='InterfaceAnnotation' />"
  "       <property type='b' name='AutoBrightness' access='readwrite'/>"
  "       <property type='b' name='AutoRotation' access='readwrite'/>"
  "       <method name='ToggleAutoRotation'/>"
  "       <method name='ToggleAutoBrightness'/>"
  "   </interface>"
  "</node>"
  ;

/** global state structure  */
struct sensor_daemon_state {
  bool auto_rotation;
  char* rotate_cmd;
  char* rotate_options[4];

  bool auto_brightness;
  char* brightness_cmd;
  uint16_t min_brightness;
  uint16_t max_brightness;

  GMainLoop* loop;
  GDBusProxy* iio_proxy;
  GDBusProxy* iio_proxy_compass;
  GDBusNodeInfo* introspection_data;
  
};


/** check if file exists*/
static inline bool file_exists(const char* path) {
  return path && access(path, R_OK) != -1;
}

/** checks if config file exists at set paths*/
static char* get_config_path(void) {
  static const char* paths[] = {
				"$PWD/sensor-daemon.conf",
				"$HOME/.config/sensor-daemon/sensor-daemon.conf",
				"$XDG_CONFIG_HOME/sensor-daemon/sensor-daemon.conf",
				"/etc/sensor-daemon/sensor-daemon.conf"

  };

  char* config_home = getenv("XDG_CONFIG_HOME");
  if (config_home ==NULL || (config_home != NULL && config_home[0]) == '\0') {
    free(config_home);
    paths[2] = "$HOME/.config/sensor-daemon/sensor-daemon.conf";
  }

  wordexp_t p;
  for (size_t i = 0; i < sizeof(paths) / sizeof(char*); i++) {
    if (wordexp(paths[i], &p, 0) == 0) {
      char* path = strdup(p.we_wordv[0]);
      wordfree(&p);
      if (file_exists(path)) {
	return path;
      }
      free(path);
	
    }
  }
  return NULL;
}

/** parses arguments into state*/
static uint8_t parse_arguments(struct sensor_daemon_state *state,
			       char* property,
			       char* value) {

  if (strcmp(property, "RotateScreenCmd") == 0) {
    state->rotate_cmd = strdup(value);
  } else if (strcmp(property, "RotateScreenOptions") == 0) {
    if (value == NULL || strcmp(value, "") == 0){
      g_error("Screen rotation options are empty\n");
      return 1;
    }
    
    uint8_t i = 0,
      line_start = 0,
      line_end = 0;
    bool done = false;    

    while (!done) {

      if (value[line_end] == ';' ||
	  value[line_end] == '\0') {
	state->rotate_options[i] =
	  strndup(&value[line_start],
		  line_end - line_start);
	line_start = line_end + 1;
	i++;
	if (i == 4) {
	  done = true;
	  
	}
      }

      if (value[line_end] == '\0') {
	  done = true;
	}
      
      line_end++;      
    }

    if (i < 3) {
      g_error("Wrong number of options for screen rotation\n");
      return 1;
    }
    
  } else if (strcmp(property, "SetBrightnessCmd") == 0) {
    state->brightness_cmd = strdup(value);
  } else if (strcmp(property, "MinBrightness") == 0) {
    uint16_t val = atoi(value);
    if (val == 0) {
      g_error("Unable to parse minimum brightness value %s\n", value);
      return 1;
    }
    state->min_brightness = val;
  } else if (strcmp(property, "MaxBrightness") == 0) {
    uint16_t val = atoi(value);
    if (val == 0) {
      g_error("Zero or incorrect maximum brightness %s\n", value);
      return 1;
    }
    state->max_brightness = val;
  } else if (strcmp(property, "AutoRotation") == 0) {
    if (strcmp(value, "true") == 0) {
      state->auto_rotation = true;
    } 
  } else if (strcmp(property, "AutoBrightness") == 0) {
    if (strcmp(value, "true") == 0) {
      state->auto_brightness = true;
    }
  }

  return 0;
}

/** reads config file and parses initial state*/
static uint8_t parse_config(char* config_file,
		     struct sensor_daemon_state* state) {

  FILE* config = fopen(config_file, "r");

  if (!config) {
    g_error("Failed to open config");
    free(config);
    return 1;
  }

  char* line = NULL;
  uint8_t line_number = 0;
  size_t line_bytes = 0;
  intmax_t n_symbols = 0;
  while ((n_symbols = getline(&line, &line_bytes, config)) != -1) {
    line_number++;
    if (line[n_symbols - 1] == '\n') {
      line[--n_symbols] = '\0';
    }

    if (line == NULL || (line != NULL && line[0] == '#')) {
      continue;
    }

    g_debug("config line #%du: %s\n", line_number, line);

    char* eq_found = strchr(line, '=');
    if (eq_found == NULL) {
      g_error("Syntax error at line %d: lines should be in property=value format \n", line_number);
      free(line);
      free(config);
      return 1;
    }

    char* prop_name = strndup(line, (eq_found - line ) * sizeof(char));
    char* prop_value = strndup(&eq_found[1], (line_bytes - (eq_found - line) - 1 ) * sizeof(char));
    g_debug("Parsed values on line #%d:\n \t%s\n\t%s\n", line_number, prop_name, prop_value);

    if (parse_arguments(state, prop_name, prop_value) != 0) {
      g_error("Syntax error at line %d: incorrect value %s for %s\n",
	      line_number,
	      prop_value,
	      prop_name);
      free(prop_value);
      free(prop_name);
      free(line);
      return 1;
    }

    free(prop_name);
    free(prop_value);

  }

  free(line);
  fclose(config);
  
  return 0;
  
}

/** set the initial toggle values*/
static void init_state_toggles(struct sensor_daemon_state* state) {
  state->auto_brightness = false;
  state->auto_rotation = false;
}

// triggering commands to change tablet properties according to sensors

static void change_orientation(const struct sensor_daemon_state* state, 
			       const char* new_orientation) {

  g_debug("in change_orientation\n"
	  "Orientation command is: %s\n"
	  "New orientation is: %s\n",
	  state->rotate_cmd,
	  new_orientation);

  
  uint8_t num_orientation = 0;
  if (g_strcmp0(new_orientation, "left-up") == 0) {
    num_orientation = 1;
  } else if (g_strcmp0(new_orientation, "right-up") == 0) {
    num_orientation = 2;
  } else if (g_strcmp0(new_orientation, "bottom-up") == 0) {
    num_orientation = 3;
  } else if (g_strcmp0(new_orientation, "normal") == 0) {
    num_orientation = 0;
  }

  uint16_t len = strlen(state->rotate_options[num_orientation]);  

  char* cmd = calloc(strlen(state->rotate_cmd) + len,
		     sizeof(char));
  sprintf(cmd,
	  state->rotate_cmd,
	  state->rotate_options[num_orientation]);
  g_debug("Rotation command is %s\n", cmd);
  system(cmd);
  free(cmd);
}

static void change_brightness(const char* brightness_cmd,
			      double new_lux,
			      uint16_t min_brightness,
			      uint16_t max_brightness)
{
  //TODO find actual lux per step
  uint32_t lux_per_step = (max_brightness - min_brightness) * 10;
  uint32_t new_brightness = new_lux / lux_per_step;

  char* cmd = calloc(strlen(brightness_cmd) + 10, sizeof(char));
  sprintf(cmd, brightness_cmd, new_brightness);
  g_debug("Brigthness command is %s\n",
	  cmd);
  system(cmd);
  free(cmd);

  
}

//watching the iio-sensor-proxy
static void
properties_changed(GDBusProxy *proxy,
                  GVariant *changed_properties,
                  GStrv invalidated_properties,
                  gpointer user_data)
{

    GVariant *v;
    GVariantDict dict;

    struct sensor_daemon_state* state= user_data;

    g_variant_dict_init(&dict, changed_properties);

    if (g_variant_dict_contains(&dict, "AccelerometerOrientation")
            && state->auto_rotation == 1) {
      
        g_debug("changing orientation\n");
        v = g_dbus_proxy_get_cached_property(state->iio_proxy, "AccelerometerOrientation");
	
        change_orientation(state,
			   g_variant_get_string(v, NULL));
        g_variant_unref(v);
    }

    if (g_variant_dict_contains(&dict, "LightLevel")
            && state->auto_brightness) {
      
        v = g_dbus_proxy_get_cached_property(state->iio_proxy, "LightLevel");
	
        change_brightness(state->brightness_cmd,
			  g_variant_get_double(v),
			  state->min_brightness,
			  state->max_brightness);
	
        g_variant_unref(v);
    }

    if (g_variant_dict_contains(&dict, "CompassHeading")) {
        v = g_dbus_proxy_get_cached_property(state->iio_proxy_compass, "CompassHeading");
        //change compass heading here
        g_variant_unref(v);
    }


}

static void
on_name_appeared(GDBusConnection *connection,
		 const gchar *name,
		 const gchar *name_owner,
		 gpointer user_data)
{

  GError *error = NULL;
  GVariant *ret = NULL;

  struct sensor_daemon_state* state = user_data;
  
  g_debug("IIO sensor proxy online\n");

  state->iio_proxy = g_dbus_proxy_new_for_bus_sync(
					    G_BUS_TYPE_SYSTEM,
					    G_DBUS_PROXY_FLAGS_NONE,
					    NULL,
					    "net.hadess.SensorProxy",
					    "/net/hadess/SensorProxy",
					    "net.hadess.SensorProxy",
					    NULL,
					    NULL
					    );
  g_signal_connect(
		   G_OBJECT(state->iio_proxy),
		   "g-properties-changed",
		   G_CALLBACK(properties_changed),
		   user_data
		   );

  if (g_strcmp0(g_get_user_name(), "geoclue") == 0) {
    state->iio_proxy_compass = g_dbus_proxy_new_for_bus_sync(
						      G_BUS_TYPE_SYSTEM,
						      G_DBUS_PROXY_FLAGS_NONE,
						      NULL,
						      "net.hadess.SensorProxy",
						      "/net/hadess/SensorProxy/Compass",
						      "net.hadess.SensorProxy.Compass",
						      NULL,
						      NULL
						      );
    g_signal_connect(
		     G_OBJECT(state->iio_proxy_compass),
		     "g-properties-changed",
		     G_CALLBACK(properties_changed),
		     NULL
		     );

  }

  //Accelerometer
  ret = g_dbus_proxy_call_sync(
			       state->iio_proxy,
			       "ClaimAccelerometer",
			       NULL,
			       G_DBUS_CALL_FLAGS_NONE,
			       -1,
			       NULL,
			       &error);

  if (!ret) {
    if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
      g_warning("Failed to claim accelerometer: %s",
		error->message);
      g_main_loop_quit(state->loop);
      return;
    }

  }
  g_clear_pointer(&ret, g_variant_unref);

  //ALS
  ret = g_dbus_proxy_call_sync(
			       state->iio_proxy,
			       "ClaimLight",
			       NULL,
			       G_DBUS_CALL_FLAGS_NONE,
			       -1,
			       NULL,
			       &error);

  if (!ret) {
    if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
      g_warning("Failed to claim light sensor: %s",
		error->message);
      g_main_loop_quit(state->loop);
      return;
    }

  }
  g_clear_pointer (&ret,
		   g_variant_unref);

  //compass
  if (g_strcmp0(g_get_user_name(), "geoclue") == 0) {
    ret = g_dbus_proxy_call_sync (state->iio_proxy,
				  "ClaimCompass",
				  NULL,
				  G_DBUS_CALL_FLAGS_NONE,
				  -1,
				  NULL,
				  &error);

    if (!ret) {
      if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
	g_warning ("Failed to claim compass: %s",
		   error->message);
	g_main_loop_quit(state->loop);
	return;
      }

    }
    g_clear_pointer (&ret,
		     g_variant_unref);
  }

}


static void
on_name_vanished(GDBusConnection *connection,
		 const gchar      *name,
		 gpointer         user_data)
{

  struct sensor_daemon_state* state = user_data;
    
  
  if (state->iio_proxy || state->iio_proxy_compass) {
    g_clear_object(&state->iio_proxy);
    g_clear_object(&state->iio_proxy_compass);
    g_debug ("IIO proxy service offline, waiting");
  }

}

//controlling the daemon's toggles

static void
on_name_lost(GDBusConnection    *conn,
             const gchar        *name,
             gpointer           user_data)
{
    g_print("Lost name %s on session bus\n", name);
}

static void
on_name_acquired(GDBusConnection    *conn,
                 const gchar        *name,
                 gpointer           user_data)
{
    g_print("Acquired name %s on session bus\n", name);
}

static void
handle_method_call(GDBusConnection         *connection,
		   const gchar             *sender,
		   const gchar             *object_path,
		   const gchar             *interface_name,
		   const gchar             *method_name,
		   GVariant                *parameters,
		   GDBusMethodInvocation   *invocation,
		   gpointer                 user_data)
{

  struct sensor_daemon_state* state = user_data;
  
  g_debug("Method %s for interface %s is called\n",
	  method_name,
	  interface_name);

  if (g_strcmp0(method_name, "ToggleAutoRotation") == 0) {
    state->auto_rotation = !state->auto_rotation;
  } else if (g_strcmp0(method_name, "ToggleAutoBrightness") == 0) {
    state->auto_brightness = !state->auto_brightness;
  }
  g_debug("new config values are:\n"
	  "\tAuto rotation:%s\n"
	  "\tAuto brightness: %s\n",
	  state->auto_rotation ? "on" : "off",
	  state->auto_brightness ? "on" : "off");

  g_dbus_method_invocation_return_value(invocation, NULL);
}

static GVariant *
handle_get_property(GDBusConnection *connection,
                    const gchar     *sender,
                    const gchar     *object_path,
                    const gchar     *interface_name,
                    const gchar     *property_name,
                    GError         **error,
                    gpointer         user_data)
{
    GVariant *ret = NULL;
    struct sensor_daemon_state* state = user_data;
    g_debug ("Get property's triggered\n");

    if (g_strcmp0(property_name, "AutoBrightness") == 0) {
        ret = g_variant_new_boolean(state->auto_brightness);
    } else if (g_strcmp0(property_name, "AutoRotation") == 0) {
        ret = g_variant_new_boolean(state->auto_rotation);
    }

    return ret;
}

static gboolean
handle_set_property(GDBusConnection *connection,
                    const gchar     *sender,
                    const gchar     *object_path,
                    const gchar     *interface_name,
                    const gchar     *property_name,
                    GVariant        *value,
                    GError         **error,
                    gpointer         user_data)
{
    struct sensor_daemon_state* state = user_data;

    if (g_strcmp0(property_name, "AutoBrightness") == 0) {
        state->auto_brightness = g_variant_get_boolean(value);
    } else if (g_strcmp0(property_name, "AutoRotation") == 0) {
        state->auto_rotation =  g_variant_get_boolean(value);
    }

    return *error == NULL;
}


static const GDBusInterfaceVTable interface_vtable =
{
    handle_method_call,
    handle_get_property,
    handle_set_property
};

static void
on_bus_acquired(GDBusConnection *conn,
                const gchar     *name,
                gpointer        user_data)
{

  struct sensor_daemon_state* state = (struct sensor_daemon_state*) user_data;
  g_print ("Bus acquired. Name's %s\n", name);
  guint  registration_id = g_dbus_connection_register_object (conn,
                                                         "/net/test/SensorDaemonObject",
							 state->introspection_data->interfaces[0],
                                                         &interface_vtable,
                                                         user_data,
                                                         NULL,
                                                         NULL);
    g_assert (registration_id > 0);

}

int main(int argc,
	 char* argv[argc + 1]) {

  struct sensor_daemon_state state = {0};

  init_state_toggles(&state);;
  
  char* config_path = get_config_path();


  if (config_path == NULL) {
    g_error("config not found");
    free(config_path);
    return EXIT_FAILURE;
  }
  
  g_debug("config path: %s\n", config_path);

  if (parse_config(config_path, &state) != 0) {
    g_error("Incorrect or missing config file\n");
    free(config_path);
    return EXIT_FAILURE;
  }

  g_debug("Current state is:\n"
	  "\tScreen Rotation: %s\n"
	  "\tBrightness: %s\n"
	  "\tMinimum brightness: %d\n"
	  "\tMaximum brightness: %d\n"
	  "\tAuto rotation: %s\n"
	  "\tAuto brightness: %s\n",
	  state.rotate_cmd,
	  state.brightness_cmd,
	  state.min_brightness,
	  state.max_brightness,
	  state.auto_rotation ? "on" : "off",
	  state.auto_brightness ? "on" : "off");

  state.introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);
  g_assert(state.introspection_data != NULL);

  guint owner_id = g_bus_own_name(G_BUS_TYPE_SESSION,
				  "net.test.SensorDaemon",
				  G_BUS_NAME_OWNER_FLAGS_NONE,
				  on_bus_acquired,
				  on_name_acquired,
				  on_name_lost,
				  &state,
				  NULL);

  guint watch_id = g_bus_watch_name(G_BUS_TYPE_SYSTEM,
				    "net.hadess.SensorProxy",
				    G_BUS_NAME_WATCHER_FLAGS_NONE,
				    on_name_appeared,
				    on_name_vanished,
				    &state,
				    NULL);
  g_print("DBus watch started\n");

  state.loop = g_main_loop_new(NULL,
			       true);
  g_main_loop_run(state.loop);

  g_bus_unown_name(owner_id);
  g_bus_unwatch_name(watch_id);
  g_dbus_node_info_unref(state.introspection_data);

  free(config_path);

  free(state.rotate_cmd);
  free(state.brightness_cmd);
  for (int i = 0; i < 4; i++) {
    free(state.rotate_options[i]);
  }
  
  return EXIT_SUCCESS;
  
}
 
